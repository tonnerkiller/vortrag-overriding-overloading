# Git Vortrag

small presentation for application programming class: https://tonnerkiller.gitlab.io/vortrag-overriding-overloading
presentation was originally meant to be on method overloading and overriding, thence the URL. Actually it is on git.

I tried to include some terminal replays recorded with script command line tool but alas, I didn't succeed.

You can find the remains scattered throughout the repository. I played around with
- [scriptreplayjs by h43z](https://github.com/h43z/scriptreplayjs)
- [scriptreplay by josch](https://gitlab.mister-muffin.de/josch/scriptreplayjs)
- [xterm.js](https://www.npmjs.com/package/xterm) and its addon `fit`
- and the vt100 emulator by Frank Bi, which is a requirement by josch's scriptreplay version, but I couldn't find a link for
 
They say failure makes you better but I still wish I had gotten the terminal replay to work... Maybe next time