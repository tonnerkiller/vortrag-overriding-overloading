/*window.addEventListener('load', () => {
    var rootElement = document.getElementById( "impress" );
    rootElement.addEventListener( "impress:stepenter", function(event) {
        var currentStep = event.target;
        //console.log( "Entered the Step Element '" + currentStep.id + "'" );
        //replay upon entering slide
        if (currentStep.id.endsWith('Term')){
          scriptreplay("scripts/"+currentStep.id+".script", "scripts/"+currentStep.id+".time", document.querySelector('#'+currentStep.id+'Div'));
        };
    });*/
    function loadScript(url, callback){
      let script = document.createElement('script')
      script.onload = callback
      script.src = url
      document.head.appendChild(script)
    }

    function loadCss(url, callback){
      let link = document.createElement('link')
      link.href =  url
      link.type = 'text/css'
      link.rel = 'stylesheet'
      link.onload = callback
      document.head.appendChild(link)
    }

    loadCss('https://cdn.jsdelivr.net/npm/xterm@3.4.1/dist/xterm.min.css');
    loadScript('https://cdn.jsdelivr.net/npm/xterm@3.4.1/dist/xterm.min.js');
    loadScript('https://cdn.jsdelivr.net/npm/xterm@3.4.1/dist/addons/fit/fit.min.js');







    window.addEventListener("load", function(evt) {
      //create terminals
      Terminal.applyAddon(fit);



      var rootElement = document.getElementById( "impress" );
      rootElement.addEventListener( "impress:stepenter", function(event) {
          var currentStep = event.target;
          //console.log( "Entered the Step Element '" + currentStep.id + "'" );
          //replay upon entering slide

          //are we on a backside slide?
          if (currentStep.id.endsWith('Term')){
            //scriptreplay("scripts/"+currentStep.id+".script", "scripts/"+currentStep.id+".time", document.querySelector('#'+currentStep.id+'Div'));
            vt = new Terminal();
            //open terminal in terminal div
            vt.open(document.getElementById(currentStep.id+'Div'),false);
            vt.resize(80, 24);
            vt.setOption("fontSize",24);
            //console.log(currentStep.id);
            speed=2.0;
            play_file("scripts/"+currentStep.id);
          };

        }, false);
        //destroying everything leaving temrinal slides
        rootElement.addEventListener("impress:stepleave", function(event) {
          var currentStep = event.target;
          if (currentStep.id.endsWith('Term')){
            vt.clear();
            vt.refresh();
            delete timer;
            delete vt;
            document.getElementById(currentStep.id+'Div').innerHTML = "";
          }
        }, false);
});

        //vt = new VT100(170, 48, "term");
        //vt.clear();
        //vt.refresh();



      //  vt.fit();
        /*vt.write(
    "\n\n\n" +
    " * This javascript terminal window is 80x24 characters, so it might be\n" +
    "   best to adjust your terminal window to that size as well using the\n" +
    "   following to check:\n" +
    "      $ watch \"tput cols; tput lines\"\n" +
    " * Start recording:\n" +
    "      $ SHELL=/bin/sh TERM=vt100 script -t typescript 2> timingfile\n" +
    " * Do your stuff and when done exit script with `exit`, `logout` or\n" +
    "   ctrl-d.\n" +
    " * To test how your recorded session looks like, use:\n" +
    "      $ scriptreplay timingfile typescript\n" +
    " * Enter `timingfile` and `typescript` into form above and hit the play\n" +
    "   button.\n");*/


        //document.getElementById("term").style.fontSize="24";









    //window.addEventListener('load', () => {
    //  scriptreplay("scripts/init.script", "scripts/init.time", document.querySelector('#initTerm'))
//});
